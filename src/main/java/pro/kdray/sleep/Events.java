package pro.kdray.sleep;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * Created by funniray on 8/13/17.
 */
public class Events implements Listener {
    @EventHandler
    public void onBedWake(PlayerBedLeaveEvent event){
        Sleeping.setPlayerInBed(event.getPlayer(),false);
        Utils.awakeFromBed(event.getPlayer());
    }
    @EventHandler
    public void onBedEnter(PlayerBedEnterEvent event){
        Sleeping.setPlayerInBed(event.getPlayer(), true);
        Utils.putToBed(event.getPlayer());
    }
    @EventHandler
    public void OnPlayerMove(PlayerMoveEvent event){
        Player player = event.getPlayer();
        if (Sleeping.getPlayerInBed(player)) {
            Sleeping.setPlayerInBed(player,false);
        }
    }
}
