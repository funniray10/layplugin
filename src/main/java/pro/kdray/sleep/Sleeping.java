package pro.kdray.sleep;

import org.bukkit.entity.Player;

import java.util.HashMap;

/**
 * Created by funniray on 8/13/17.
 */
public class Sleeping {
    private static HashMap<Player,Boolean> inBed = new HashMap<>();

    public static HashMap<Player, Boolean> getInBed() {
        return inBed;
    }

    public static void setInBed(HashMap<Player, Boolean> newInBed) {
        inBed = newInBed;
    }

    public static void setPlayerInBed(Player player, boolean bool){
        inBed.put(player,bool);
        if (!bool){
            Utils.awakeFromBed(player);
        }else{
            Utils.putToBed(player);
        }
    }

    public static boolean getPlayerInBed(Player player){
        inBed.putIfAbsent(player,false);
        return inBed.get(player);
    }

    public static boolean toggleInBed(Player player){
        setPlayerInBed(player,!getPlayerInBed(player));
        return !getPlayerInBed(player);
    }
}
