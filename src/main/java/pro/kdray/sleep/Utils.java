package pro.kdray.sleep;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.BlockPosition;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntity.PacketPlayOutRelEntityMove;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by funniray on 8/13/17.
 */
public class Utils {

    @SuppressWarnings("deprecation")
    public static void putToBed(final Player player) {
        Location bedPos = player.getLocation().clone();
        bedPos.setY(1);
        /* Block change required for the bed packet */
        //player.sendBlockChange(player.getLocation(), Material.BED_BLOCK, (byte) 0);

        //Send packet using ProtocolLib
        PacketContainer packet = new PacketContainer(PacketType.Play.Server.BED);
        packet.getIntegers().write(0, player.getEntityId());
        packet.getBlockPositionModifier().write(0, new BlockPosition(bedPos.toVector()));
        PacketPlayOutRelEntityMove packettp = new PacketPlayOutRelEntityMove(
                player.getEntityId(), (byte) 0, (byte) (-60.8), (byte) 0, false);
        List<Player> players = new ArrayList<>(Bukkit.getServer().getOnlinePlayers());
        players.remove(player);
        for (int i = 0; i < players.size(); i++) {
            try {
                Player playeri = players.get(i);
                playeri.sendBlockChange(bedPos, Material.BED_BLOCK, (byte) getDirection(Math.abs(player.getLocation().getYaw())));
                playeri.sendBlockChange(getBlockBefore(bedPos,getDirection(Math.abs(player.getLocation().getYaw()))), Material.BED_BLOCK, (byte) (getDirection(Math.abs(player.getLocation().getYaw()))+8));
                ProtocolLibrary.getProtocolManager().sendServerPacket(playeri, packet);
                ((CraftPlayer) playeri).getHandle().playerConnection.sendPacket(packettp);
            } catch (InvocationTargetException e) {
                Bukkit.getServer().getLogger().severe(e.getMessage());
            }
        }
        player.sendMessage("You are now laying down!");
        //Bukkit.getServer().getScheduler().runTaskLater(Sleep.sleep, new Runnable() {
        //    @Override
        //    public void run() {
        //        int relx = player.getLocation().getChunk().getX();
        //        int relz = player.getLocation().getChunk().getZ();
        //        ArrayList<Chunk> chunks = new ArrayList<>();
        //        for (int x=-5;x<=5;x++){
        //          for (int z=-5;z<=5;z++){
        //              new PacketMapChunk(player.getWorld().getChunkAt(relx+x,relz+z)).send(player);
        //          }
        //        }
        //    }
        //},5L);
    }

    @SuppressWarnings("deprecation")
    public static void awakeFromBed(final Player player) {
		/* Remove block change */
		Location bedPos = player.getLocation();
		bedPos.setY(1);

        //Send packet using ProtocolLib
        PacketContainer packet = new PacketContainer(PacketType.Play.Server.ANIMATION);
        packet.getIntegers().write(0, player.getEntityId());
        packet.getIntegers().write(1, 2);
        List<Player> players = new ArrayList<>(Bukkit.getServer().getOnlinePlayers());
        players.remove(player);
        for (int i = 0; i < players.size(); i++) {
            try {
                players.get(i).sendBlockChange(bedPos, bedPos.getBlock().getType(), (byte) 0);
                players.get(i).sendBlockChange(getBlockBefore(bedPos,getDirection(Math.abs(player.getLocation().getYaw()))), getBlockBefore(bedPos,getDirection(Math.abs(player.getLocation().getYaw()))).getBlock().getType(), (byte) 0);
                ProtocolLibrary.getProtocolManager().sendServerPacket(players.get(i), packet);
            } catch (InvocationTargetException e) {
                Bukkit.getServer().getLogger().severe(e.getMessage());
            }
        }
        player.sendMessage("You are now longer laying down!");
    }
    public static int getDirection(double rot) {
        if (0 <= rot && rot < 67.5) {
            return 0;
        } else if (67.5 <= rot && rot < 157.5) {
            return 1;
        } else if (157.5 <= rot && rot < 247.5) {
            return 2;
        } else if (247.5 <= rot && rot < 337.5) {
            return 3;
        } else if (337.5 <= rot && rot < 360.0) {
            return 0;
        } else {
            return 0;
        }
    }
    public static Location getBlockBefore(Location location,int facing){
        switch (facing){
            case 0:
                location.add(0,0,-1);
                break;
            case 1:
                location.add(-1,0,0);
                break;
            case 2:
                location.add(0,0,1);
                break;
            case 3:
                location.add(1,0,0);
                break;
            default:
                location.add(0,0,0);
                break;
        }
        return location;
    }
}
//TODO: Remove this line