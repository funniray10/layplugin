package pro.kdray.sleep;

import net.minecraft.server.v1_12_R1.PacketPlayOutMapChunk;
import org.bukkit.craftbukkit.v1_12_R1.CraftChunk;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

/**
 * Created by funniray on 8/14/17.
 */
public class PacketMapChunk {

    private final net.minecraft.server.v1_12_R1.Chunk chunk;

    public PacketMapChunk(final org.bukkit.Chunk chunk) {
        this.chunk = ((CraftChunk)chunk).getHandle();
    }

    public final void send(final Player player) {
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(new PacketPlayOutMapChunk(chunk,20));
    }

}