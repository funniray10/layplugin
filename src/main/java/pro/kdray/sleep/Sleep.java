package pro.kdray.sleep;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public final class Sleep extends JavaPlugin {
    public static Plugin sleep;
    @Override
    public void onEnable() {
        // Plugin startup logic
        sleep = this;
        getServer().getPluginManager().registerEvents(new Events(),this);
        getCommand("lay").setExecutor(new CommandHandler());
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
